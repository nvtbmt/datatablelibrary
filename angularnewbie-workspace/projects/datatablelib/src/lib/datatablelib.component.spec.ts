import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatatablelibComponent } from './datatablelib.component';

describe('DatatablelibComponent', () => {
  let component: DatatablelibComponent;
  let fixture: ComponentFixture<DatatablelibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatatablelibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatatablelibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
