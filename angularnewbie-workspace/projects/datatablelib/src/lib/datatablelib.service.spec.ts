import { TestBed } from '@angular/core/testing';

import { DatatablelibService } from './datatablelib.service';

describe('DatatablelibService', () => {
  let service: DatatablelibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatatablelibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
