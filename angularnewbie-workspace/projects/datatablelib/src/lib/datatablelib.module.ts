import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DatatablelibComponent } from './datatablelib.component';

import {TableModule, SortableColumn, SortIcon} from 'primeng-lts/table';
import {MultiSelectModule} from 'primeng-lts/multiselect';
import {ButtonModule} from 'primeng/button';


@NgModule({
  declarations: [DatatablelibComponent],
  imports: [
    CommonModule,
    TableModule,
    MultiSelectModule,
    FormsModule,
    ButtonModule
  ],
  exports: [DatatablelibComponent]
})
export class DatatablelibModule { }
