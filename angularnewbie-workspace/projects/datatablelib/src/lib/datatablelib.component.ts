import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpEventType, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { catchError, map, retry, takeUntil } from 'rxjs/operators';
import { ColumnItems } from './interfaces/column-items';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'lib-datatablelib',
  templateUrl: './datatablelib.component.html',
  styles: [
    '.left-group { display: flex; }',
    '.left-group .export { display: flex; margin-left: 1em; }',
    '.left-group .export .button { margin-right: 0.5em; }'
  ]
})
export class DatatablelibComponent implements OnInit, OnDestroy, OnChanges {

  @Input() endPoint: string;
  @Input() pickedProperties: ColumnItems[];
  @Input() caption: string;
  @Input() searchTool: boolean;
  @Input() columnToggle: boolean;
  // @Input() sortableColumn: boolean;
  @Input() paging: boolean;
  @Input() rowPaging: number;
  @Input() verticalScrollable: boolean;
  @Input() scrollHeight: number;
  @Input() exportType: string[];

  dataObjects: any[];
  properties: ColumnItems[];
  AvailableExportType: {[type: string]: boolean};
  choosenExportType: string[];
  private ngUnsubscribe = new Subject();

  constructor(private http: HttpClient) {
    /* tslint:disable:no-string-literal */
    this.AvailableExportType = {};
    this.AvailableExportType['CSV'] = true;
    this.AvailableExportType['EXCEL'] = true;
    this.AvailableExportType['PDF'] = true;
    /* tslint:enable:no-string-literal */
    this.choosenExportType = [];
  }

  ngOnInit(): void {
    this.http.get<any[]>(this.endPoint)
      .pipe(
        retry(3),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((data) => {
        this.dataObjects = data;
      });
  }

  ngOnDestroy(): void{
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnChanges(): void{
    this.properties = this.pickedProperties;
    if (this.exportType !== undefined){
      this.exportType.forEach(type => {
        if (this.AvailableExportType[type.toUpperCase()]){
          this.choosenExportType.push(type.toUpperCase());
        }
      });
    }
  }

  exportPdf() {
    const doc = new jsPDF();
    const result = doc.autoTableHtmlToJson(document.getElementById('datatable'));
    doc.autoTable(result.columns, result.data);
    doc.save(this.caption.trim() !== undefined ? this.caption.trim() : 'tableName' + '.pdf');
  }

  exportExcel() {
    import('xlsx').then((xlsx) => {
      const worksheet = xlsx.utils.json_to_sheet(this.dataObjects);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, this.caption.trim() !== undefined ? this.caption.trim() : 'tableName');
    });
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then((FileSaver) => {
      const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      const EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE,
      });
      FileSaver.saveAs(
        data,
        fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
      );
    });
  }

}
