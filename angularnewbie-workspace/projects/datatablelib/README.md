# Datatablelib

Use this library to quikly show data in a wonderful table

# Dependency

1. Angular v9
2. PrimeNG v9

# Usage

- Use '<lib-datatablelib [...inputs]></lib-datatablelib>' tag every where
- Where '[...inputs]' are:
    + endPoint (string): Url Enpoint where data is crawlable
    + pickedProperties (ColumnItems[]): Properties of data to showing. Use 'import { ColumnItems } from "datatablelib"';
    + searchTool (boolean): Enable search tool
    + columnToggle (boolean): Enable column toggle tool
    + paging (boolean): Enable paging
    + rowPaging (number): rows per page
    + verticalScrollable (boolean): vertical scrollable (disable if paging is true)
    + scrollHeight (css height): scroll height
    + exportType (string[]): export types to export table. 'pdf', 'csv', 'excel' are available.
    + caption (string): table caption